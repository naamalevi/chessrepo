//
// Created by Natali on 06/01/2018.
//

#include "Bishop.h"
#define BISHOP_CODE "\u265D"

/**
 * Bishop constructor
 * @param color bishops color
 * @param row row coordinate
 * @param column column coordinate
 */
Bishop::Bishop
        (const Color color, const int row, const int column) : Piece(color, row, column)
{
    _representationCode = BISHOP_CODE;
}

/**
 * Bishop copy constructor (deep copy)
 * @param otherB bishop to copy
 */
Bishop::Bishop(const Bishop &otherB) : Piece(otherB._myColor, otherB._row, otherB._col)
{
    _representationCode = otherB._representationCode;
    _ifFirst = otherB._ifFirst;
}

/**
 * This function checks if new coordinates of current piece makes a legit movement on the board
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return true if it is a legit move, false otherwise
 */
bool Bishop::isLegitMove(const Board& board, const int row, const int column) const
{
    int rowDiff = (row > _row) ? (row - _row) : (_row - row);
    int colDiff = (column > _col) ? (column - _col) : (_col - column);
    return (rowDiff == colDiff) && !_checkDiagonal(board, row, column);
}

/**
 * This function changes current coordinates of any piece on the board to the new given position
 * @param row new row coordinate
 * @param column ne column coordinate
 * @return true if change ended successfully
 */
bool Bishop::makeMove(const int row, const int column)
{
    _ifFirst = false;
    _row = row;
    _col = column;
    return true;
}

/**
 * Creates identical new bishop (saved on the heap)
 * @return a pointer to the new bishop.
 */
Bishop* Bishop::clone() const
{
    Bishop* cBishopPtr = new Bishop(*this);
    return cBishopPtr;
}