//
// Created by Natali on 06/01/2018.
//

#include "Rook.h"
#define ROOK_CODE "\u265C"

/**
 * Rook constructor
 * @param color rook color
 * @param row int represents row coordinate
 * @param column int represents column coordinate
 */
Rook::Rook(const Color color, const int row, const int column) : Piece(color, row, column)
{
    _representationCode = ROOK_CODE;
}

/**
 * Rook copy constructor
 * @param otherR other rook to be cloned
 */
Rook::Rook(const Rook &otherR) : Piece(otherR._myColor, otherR._row, otherR._col)
{
    _ifFirst = otherR._ifFirst;
    _representationCode = otherR._representationCode;
}

/**
 * This function checks if new coordinates of current piece makes a legit movement on the board
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return true if it is a legit move, false otherwise
 */
bool Rook::isLegitMove(const Board& board, const int row, const int column) const
{
    if(row == _row)
    {
        return !_checkHorizontal(board, column);
    }
    else if (column == _col)
    {
        return !_checkVertical(board, row);
    }
    else
    {
        return false;
    }
}

/**
* This function changes current coordinates of any piece on the board to the new given position
* @param row new row coordinate
* @param column ne column coordinate
* @return true if change ended successfully
*/
bool Rook::makeMove(const int row, const int column)
{
    _ifFirst = false;
    _row = row;
    _col = column;
    return true;
}

/**
 * Creates identical new rook (saved on the heap)
 * @return a pointer to the new rook.
 */
Rook* Rook::clone() const
{
    Rook* cRookPtr = new Rook(*this);
    return cRookPtr;
}