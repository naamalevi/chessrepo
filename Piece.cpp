//
// Created by Natali on 02/01/2018.
//

#include "Piece.h"
#define STEP 1
#define MIN_INDEX 0
#define MAX_INDEX 7

#define ASCII_ESCAPE "\33["
#define RESET_COLOR "0"
#define WHITE_TEXT "37"
#define BLACK_TEXT "30"
#define GREEN_BG 42
#define BLUE_BG 46
#define RED_BG 41
#define E_COLOR_CODE "m"

/**
 * Class that represents Piece on the chess board.
 * this class is abstract and cannot have instances, it's mainly used as a skeleton for "
 * real pieces" like pawn, queen, king etc
 */
Piece::Piece
        (Color color = EMPTY_SPOT, const int row = EMPTY_DEFAULT, const int column = EMPTY_DEFAULT)
{
    _myColor = color;
    _row = row;
    _col = column;
    _ifFirst = true;
    _backGround = BLUE_BG; // just a default that will be changed later
    _colorCode = (color == BLACK) ? BLACK_TEXT : WHITE_TEXT;
}

/**
 * Checks all the squares between the piece to it's destination on the board,
 * only if the movement is Diagonal
 * @param board chess board
 * @param destRow int represents the destination's row
 * @param destCol int represents the destination's column
 * @return true if some piece stands between current position and destination (not include the
 *          destination square itself), false otherwise (if the way is clear)
 */
bool Piece::_checkDiagonal(const Board& board, const int destRow, const int destCol) const
{
    int verticalStep = (destRow > _row) ? STEP : -STEP;
    int horizontalStep = (destCol > _col) ? STEP : -STEP;
    int currRow = _row, currCol = _col;
    while((currRow != destRow) && (currCol != destCol))
    {
        currRow += verticalStep;
        currCol += horizontalStep;
        // this condition allows the last coordinate to be populates (in order to allow eating)
        // false supposed to be returned only if cells between the start and the target is populated
        if((board(currRow, currCol)->color() != EMPTY_SPOT) && (currRow != destRow))
        {
            return true;
        }
    }
    return false;
}

/**
  * Checks all the squares between the piece to it's destination on the board,
  * only if the movement is vertical
  * @param board chess board
  * @param destRow int represents the destination's row. the only coordinate that is changed is
  *          the row because the movement is vertical, the column stays the same
  * @return true if some piece stands between current position and destination (not include the
  *          destination square itself), false otherwise (if the way is clear)
  */
bool Piece::_checkVertical(const Board& board, const int destRow) const
{
    int verticalStep = (destRow > _row) ? STEP : -STEP;
    int currRow = _row;
    while(currRow != destRow)
    {
        currRow += verticalStep;

        // if out of the board limits - relevant to the Pawn case
        if ((currRow > MAX_INDEX) || (currRow < MIN_INDEX))
        {
            return false;
        }
        // the condition ignores cases with populated lase coordinate, in order to allow eating
        if(((board(currRow, _col)->_myColor != EMPTY_SPOT) && (currRow!= destRow)))
        {
            return true;
        }
    }
    return false;
}

/**
 * Checks all the squares between the piece to it's destination on the board,
 * only if the movement is Horizontal
 * @param board chess board
 * @param destCol int represents the destination's column. the only coordinate that changed is
 *          the column because the movement is horizontal, the row stays the same
 * @return true if some piece stands between current position and destination (not include the
 *          destination square itself), false otherwise (if the way is clear)
 */
bool Piece::_checkHorizontal(const Board& board, const int destCol) const
{
    int horizontalStep = (destCol > _col) ? STEP : -STEP;
    int currCol = _col;
    while(currCol != destCol)
    {
        currCol += horizontalStep;
        // the condition ignores cases with populated lase coordinate, in order to allow eating
        if((board(_row, currCol)->_myColor != EMPTY_SPOT) && (currCol != destCol))
        {
            return true;
        }
    }
    return false;
}

/**
 * Output operator, prints the piece into the given writable-stream
 * @param os output stream
 * @param p piece to print
 * @return reference to the given output stream
 */
std::ostream &operator << (std::ostream &os, const Piece *p)
{
    return os << ASCII_ESCAPE << p -> _colorCode << ";" << p -> _backGround <<
       E_COLOR_CODE << p -> _representationCode << ASCII_ESCAPE << RESET_COLOR << E_COLOR_CODE;
}

/**
 * sets the pieces current backGround
 * @param backGround int represents the color of the background in unicode
 */
void Piece::setBackground(const int backGround)
{
    _backGround = backGround;
}

/**
 * @return Gets the code for string representation of the piece
 */
std::string Piece::getRepresentation() const
{
    return _representationCode;
}

/**
 * Gets the column coordinate of the piece
 * @return int represent the column coordinate
 */
int Piece::col() const
{
    return _col;
}

/**
 * Gets the row coordinate of the piece
 * @return int represent the row coordinate
 */
int Piece::row() const
{
    return _row;
}

/**
 * @return true if it's piece's first move, false otherwise
 */
bool Piece::isFirstMove() const
{
    return _ifFirst;
}

/**
 * @return Gets the piece's color
 */
Color Piece::color() const
{
    return _myColor;
}
