//
// Created by Natali on 06/01/2018.
//

#ifndef CHESS_ROOK_H
#define CHESS_ROOK_H

#include "Piece.h"

/**
 * Class represents a rook piece on chess board
 */
class Rook : public Piece
{
public:
    /**
     * Rook constructor
     * @param color rook color
     * @param row int represents row coordinate
     * @param column int represents column coordinate
     */
    Rook(const Color color, const int row, const int column);

    /**
     * Rook copy constructor
     * @param otherR other rook to be cloned
     */
    Rook(const Rook& otherR);

    /**
     * This function checks if new coordinates of current piece makes a legit movement on the board
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return true if it is a legit move, false otherwise
     */
    bool isLegitMove(const Board& board, const int row, const int column) const;

    /**
    * This function changes current coordinates of any piece on the board to the new given position
    * @param row new row coordinate
    * @param column ne column coordinate
    * @return true if change ended successfully
    */
    bool makeMove(const int row, const int column);

    /**
     * Creates identical new rook (saved on the heap)
     * @return a pointer to the new rook.
     */
    Rook* clone() const override;
};

#endif //CHESS_ROOK_H
