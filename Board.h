//
// Created by Natali on 05/01/2018.
//

#ifndef CHESS_BOARD_H
#define CHESS_BOARD_H

#include <iostream>
#include "Piece.h"

#define EMPTY_STR ""
class Piece;

#define SIZE 8
#define NUM_OF_KINGS 2

/**
 * Class that represents an chess board, the locations of the tools and manipulations on it.
 */
class Board
{
public:

    /**
     * Board constructor.
     * Initializes the board according to the first state of the chess board.
     */
    Board();

    /**
     * Board destructor.
     * Releases the inner located memory.
     */
    ~Board();

    /**
     * Copy constructor. Creates a new identical Board as the given board.
     * @param boardToCopy - A board to deep copy.
     */
    Board(const Board& boardToCopy);

    /**
     * Overloads the assignment operator.
     * Do deep assignment, makes a deep copy of the board.
     * @param other - A board to deep copy.
     * @return - A ref to the given board, contains the deep copied data.
     */
    Board& operator=(const Board& other);

    /**
     * Overload the () operator, returns a pointer to the piece ( chess tool or empty spot)
     * that is held in the given location.
     * @param row - An int representing the row location.
     * @param col- An int representing the column location.
     * @return a pointer to the tool that is held is that location.
     */
    Piece* operator()(const int &row, const int &col) const ;

    /**
      * Overload the << operator, prints the board in the classic board format, when blue is the top
      * right piece color. In case of check or win situation the massages are printed below the
      * board.
      * @param board - A raf to board to print.
      * @return  A ref to the given ostream
      */
    friend std::ostream& operator << (std::ostream& os, const Board& board);

    /**
     * Prints an illegal message to the board, (white letters on red background).
     */
    void printIllegal() const;

    /**
     * Updates the winnerName field.
     * @param winner A string representing the winner's name.
     */
    void setWinner(const std::string &winner);

    /**
     * Updates the isCheck flag to represent false - no check.
     */
    void resetCheck();

    /**
     * A getter - returns the winner's name.
     * @return a string thet representing the winner's name.
     */
    std::string getWinner() const;

    /**
     * A getter - returns the current isCheck status.
     * @return A bool represents if there is a check on the board.
     */
    bool getIsCheck() const;

    /**
     * A getter - returns the current isCheckMate status.
     * @return A bool represents if there is a checkMate on the board.
     */
    bool getIsCheckMate() const;

    /**
     * A getter - returns the color of the winner.
     * @return An boolean represents the color of the winner - true for black, 0 for white.
     */
    bool getWinnerColor() const;

    /**
     * Returns the row location of the king that it asked to.
     * @param isBlack - A boolean representing the color of the wanted king, true for black,
     * false for white.
     * @return - An int representing the row location of the king in the given color.
     */
    int _getKingRow(const bool &isBlack) const;

    /**
     * Returns the column location of the king that it asked to.
     * @param isBlack - A boolean representing the color of the wanted king, true for black,
     * false for white.
     * @return - An int representing the column location of the king in the given color.
     */
    int _getKingCol(const bool &isBlack) const;

    /**
     * Control the move operation.
     * If the move is legal, updates the game state: board, check, chackmate and winner's color.
     * otherwise prints a massage to the board.
     * @param sourceRow - const ref to int, row location of the tool that the player want to move.
     * @param sourceCol - const ref to int, column location of the tool that the player want to move
     * @param destRow - const ref to int, row location that the player want to move its tool to.
     * @param destCol - const ref to int, column location that the player want to move its tool to.
     * @param isShortCast - A ref to boolean representing if the given move is a short castling.
     * @param isLongCast - A ref to boolean representing if the given move is a long castling.
     * @param colorTurn - A ref to boolean representing the current color thet plays, 1 for black,
     * 0 for white.
     * @return True if this castling is legal for this conditions, false otherwise.
     */
    bool moveOperation(const int &sourceRow, const int &sourceCol, const int &destRow,
                       const int &destCol, const bool &isShortCast, const bool &isLongCast,
                       const bool &colorTurn);

private:

    /**
     * Checks if the given castling type is supported according to the board state (assuming the
     * first checks had already done, there are tools in the respectively locations, and these tools
     * are in the current turn color. Also, the king is not threatened.
     * The consequence checks are not be checked in this method.
     * @param row - An int representing the row that the wanted castling should be done in.
     * @param shortCastling - An boolean, true for short castling, false for long one.
     * @return True if this castling is legal for this conditions, false otherwise.
     */
    bool _isLegitCastling(const int &row, const bool &shortCastling) const;

    /**
     * Moves a castling if it legal (without treatment in the consequences)
     * @param row - An int representing the row that the wanted castling should be done in.
     * @param shortCastling - An boolean, true for short castling, false for long one.
     * @return True if this castling is legal and was done, false otherwise.
     */
    bool _moveCastling(const int &row, const bool &shortCastling);

    /**
     * Moves a normal move if it legal (without treatment in the consequences)
     * @param iRow - const int, row location of the tool that the player want to move.
     * @param iCol - const int, column location of the tool that the player want to move.
     * @param dRow - const to int, row location that the player want to move its tool to.
     * @param dCol - const to int, column location that the player want to move its tool to.
     * @return True if this move is legal and was done, false otherwise.
     */
    bool _moveTools(const int &iRow, const int &iCol, const int &dRow, const int &dCol);

    /**
     * Checks if the move can be done, according to the game rules (with no treatment to the
     * consequences), in case of legal move - updates the board.
     * return true if all the conditions to non-castling move are carried on (there is a tool in
     * that location, the tool is in the current turn color, the destination of the move is a new
     * location on the board, with no self color tool that stands there, and the move is valid
     * for the chosen type of tool.
     * @param sourceRow - const ref to int, row location of the tool that the player want to move.
     * @param sourceCol - const ref to int, column location of the tool that the player want to move.
     * @param destRow - const ref to int, row location that the player want to move its tool to.
     * @param destCol - const ref to int, column location that the player want to move its tool to.
     * @return true if the move is legal (with no treatment to the consequences) and was done on the
     * given board
     */
    bool _confirmNormalMove(const int &sourceRow, const int &sourceCol,
                            const int &destRow, const int &destCol, bool turnColor);

    /**
     * Checks if the short castling can be done, according to the game rules (with no treatment to
     * the consequences), in case of legal move - updates the board.
     * return true if all the conditions to short castling are carried on (the king and the rook
     * that closer to the king are there, didn't moved before, there are no tools between them, and
     * the king is not threatened.
     * @param turnColor - An boolean represents the color that plays now, 1 for B, 0 for W.
     * @return true if the castling is legal (with no treatment to the consequences) and was done
     * on the given board.
     */
    bool _confirmShortCastlingMove(bool turnColor);

    /**
     * Checks if the long castling can be done, according to the game rules (with no treatment to
     * the consequences), in case of legal move - updates the board.
     * return true if all the conditions to long castling are carried on (the king and the rook
     * that further to the king are there, didn't moved before, there are no tools between them, and
     * the king is not threatened.
     * @param turnColor - An boolean represents the color that plays now, 1 for B, 0 for W.
     * @return true if the castling is legal (with no treatment to the consequences) and was done
     * on the given board.
     */
    bool _confirmLongCastlingMove(bool turnColor);

    /**
     * Checks if the given location on the board is threatened, that is to say can be eaten by any
     * tool of the opponent chess tools.
     * @param row - An int representing the row location that we want to check if threatened.
     * @param col - An int representing the column location that we want to check if threatened.
     * @param isBlackTurn - A boolean representing the current turn-color, true for black, false for
     * white.
     * @return A boolean, true if the piece is threatened, false otherwise.
     */
    bool _checkIfThreatened(const int &row, const int &col, bool isBlackTurn) const;

    /**
     * Check if all the empty spots around the given spot (within the board limits) are threatened by
     * the opponent tools.
     * @param row - An int representing the row location that we want to check if all the empty
     * spots around it are threatened.
     * @param col - An int representing the column location that we want to check if empty
     * spots around it are threatened.
     * @param isBlackTurn - A boolean representing the current turn-color, true for black, false for
     * white.
     * @return A boolean, true if all the empty spots around it are threatened, false otherwise.
     */
    bool _checkIfCheckMate(const int &row, const int &col, bool isBlackTurn) const;


    Piece* _board[SIZE][SIZE];
    Piece* _kingLocations[NUM_OF_KINGS]; // contains the two kings. the white in [0], the black in [1]
    std::string _winnerName;
    bool _isCheck;
    bool _isCheckMate;
    bool _winnersColor;

};

#endif //CHESS_BOARD_H
