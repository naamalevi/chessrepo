//
// Created by Natali on 05/01/2018.
//

#include "EmptySpot.h"
#define EMPTY_PRINT " "
#define EMPTY_COLOR "0"

/**
 * Empty spot constructor
 * @param color empty color
 * @param row int represents row coordinate
 * @param column int represents column coordinate
 */
EmptySpot::EmptySpot
        (const Color color, const int row, const int column): Piece(color, row, column)
{
    _ifFirst = false;
    _representationCode = EMPTY_PRINT;
    _colorCode = EMPTY_COLOR;
}

/**
 * Movement of empty spot is not allowed, so this function will always return false
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return false (always)
 */
bool EmptySpot::isLegitMove(const Board& /*board*/, const int /*row*/, const int /*column*/) const
{
    return false;
}

/**
 * This function changes current coordinates of any piece on the board to the new given position
 * @param row new row coordinate
 * @param column ne column coordinate
 * @return true if change ended successfully
 */
bool EmptySpot::makeMove(const int row, const int column)
{
    _row = row;
    _col = column;
    return true;
}

/**
 * Creates an identical new EmptySpot (saved on the heap)
 * @return a pointer to the new EmptySpot.
 */
EmptySpot* EmptySpot::clone()const
{
    EmptySpot* cEmptySpotPtr = new EmptySpot(_myColor, _row, _col);
    return cEmptySpotPtr;
}