//
// Created by Natali on 05/01/2018.
//

#ifndef CHESS_EMPTYSPOT_H
#define CHESS_EMPTYSPOT_H

#include "Piece.h"

/**
 * Class that represents empty spot on ther board
 */
class EmptySpot : public Piece
{
public:
    /**
     * Empty spot constructor
     * @param color empty color
     * @param row int represents row coordinate
     * @param column int represents column coordinate
     */
    EmptySpot(const Color color, const int row, const int column);

    /**
     * This function changes current coordinates of any piece on the board to the new given position
     * @param row new row coordinate
     * @param column ne column coordinate
     * @return true if change ended successfully
     */
    bool makeMove(const int row, const int column);

    /**
     * Movement of empty spot is not allowed, so this function will always return false
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return false (always)
     */
    bool isLegitMove(const Board& board, const int row, const int column) const;

    /**
     * Creates an identical new EmptySpot (saved on the heap)
     * @return a pointer to the new EmptySpot.
     */
    EmptySpot* clone() const override ;
};


#endif //CHESS_EMPTYSPOT_H
