//
// Created by naama on 07/01/2018.
//

#ifndef CPP_EX2_GAME_H
#define CPP_EX2_GAME_H

#include <iostream>
#include "Piece.h"

#define NUM_OF_PLAYERS 2

/**
 * Class that represents one chess game.
 * Contains the game's state.
 */
class Game
{
public:

    /**
     * Game's constructor (the default).
     */
    Game() = default;

    /**
     * Runs the game
     * @return 0 in case of success
     */
    int runGame();

private:

    /**
     * Gets the players' names from the standard input. Saves them into the playersNames field.
     */
    void _getPlayersNames();

    /**
     * Gets the desired move from the player.
     * o-o for short castling.
     * o-o-o for long castling.
     * For normal move, the move is given by the following format: "ABCD". when A is the source column
     * B is the source row, C is the destination column, and D is the destination row.
     * Rows are represented by numbers 1-8.
     * Columns are represented by letters A-H.
     * All the following parameters updates during the method:
     * @param sourceRow - ref to int, row location of the tool that the player want to move.
     * @param sourceCol - ref to int, column location of the tool that the player want to move.
     * @param destRow - ref to int, row location that the player want to move its tool to.
     * @param destCol - ref to int, column location that the player want to move its tool to.
     * @param isShortCast - A ref to boolean representing if the given move is a short castling.
     * @param isLongCast - A ref to boolean representing if the given move is a long castling.
     * @return 0 in success, 1 otherwise.
     */
    bool _getPlayerRequest(int &sourceRow, int &sourceCol, int &destRow, int &destCol,
                           bool &isShortCast, bool &isLongCast) const;

    bool _colorTurn = false; // represents the current turn, false for white, true for black.
    std::string _playersName[NUM_OF_PLAYERS];
    // contains the players' names, the white in the first index the black in the second one.
    Board _gameBoard;
};


#endif //CPP_EX2_GAME_H
