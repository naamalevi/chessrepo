//
// Created by naama on 07/01/2018.
//

#include "Game.h"
#include <regex>

#define ASK_WHITE_NAME "Enter white player name:"
#define ASK_BLACK_NAME "Enter black player name:"
#define ASK_MOVE ": Please enter your move:"

#define ASCII_LETTER_TO_NUM 65
#define ASCII_INT_TO_NUM 49
#define LAST_COLUMN 7

#define PROPER_INPUT "( +)?([A-H])([1-8])([A-H])([1-8])( +)?"
#define SHORT_CASTLING "o-o"
#define LONG_CASTLING "o-o-o"
#define SPACE ' '

#define WHITE_PLAYER 0
#define BLACK_PLAYER 1

#define RAN_SUCCESSFULLY 0

/**
 * Gets the players' names from the standard input. Saves them into the playersNames field.
 */
void Game::_getPlayersNames()
{
    std::cout << ASK_WHITE_NAME << std::endl;
    std::getline(std::cin, _playersName[WHITE_PLAYER]);
    std::cout << ASK_BLACK_NAME << std::endl;
    std::getline(std::cin, _playersName[BLACK_PLAYER]);
}


/**
 * Gets the desired move from the player.
 * o-o for short castling.
 * o-o-o for long castling.
 * For normal move, the move is given by the following format: "ABCD". when A is the source column
 * B is the source row, C is the destination column, and D is the destination row.
 * Rows are represented by numbers 1-8.
 * Columns are represented by letters A-H.
 * All the following parameters updates during the method:
 * @param sourceRow - ref to int, row location of the tool that the player want to move.
 * @param sourceCol - ref to int, column location of the tool that the player want to move.
 * @param destRow - ref to int, row location that the player want to move its tool to.
 * @param destCol - ref to int, column location that the player want to move its tool to.
 * @param isShortCast - A ref to boolean representing if the given move is a short castling.
 * @param isLongCast - A ref to boolean representing if the given move is a long castling.
 * @return 0 in success, 1 otherwise.
 */
bool Game::_getPlayerRequest(int &sourceRow, int &sourceCol, int &destRow, int &destCol,
                             bool &isShortCast, bool &isLongCast)const
{
    unsigned long firstNotSpace;
    std::string command;
    std::cout << _playersName[_colorTurn] << ASK_MOVE << std::endl;
    std::getline(std::cin, command);
    if (std::regex_match(command, std::regex(PROPER_INPUT)))
    {
        firstNotSpace = command.find_first_not_of(SPACE);
        sourceCol = LAST_COLUMN - (command[firstNotSpace] - ASCII_LETTER_TO_NUM);
        sourceRow = command[firstNotSpace + 1] - ASCII_INT_TO_NUM;
        destCol = LAST_COLUMN - (command[firstNotSpace + 2] - ASCII_LETTER_TO_NUM);
        destRow = command[firstNotSpace + 3] - ASCII_INT_TO_NUM;
        return true;
    }
    if (command == SHORT_CASTLING)
    {
        isShortCast = true;
        return true;
    }
    if (command == LONG_CASTLING)
    {
        isLongCast = true;
        return true;
    }
    return false; // not valid input
}


/**
 * Runs the whole game.
 * First, ask for players name, then ask to moves and play them until there is a winner.
 * @return 0 in success, 1 otherwise.
 */
int Game::runGame()
{
    int sRow, sCol, dRow, dCol;
    bool shortCast, longCast, checkInLastTurn; // checkInLastTurn is used to warn for check
    // also after illegal move.
    _getPlayersNames();
    checkInLastTurn = false;
    while (!_gameBoard.getIsCheckMate())
    {
        std::cout << _gameBoard;
        shortCast = longCast = false;
        if (_getPlayerRequest(sRow, sCol, dRow, dCol, shortCast, longCast))
        {
            if(_gameBoard.moveOperation(sRow, sCol, dRow, dCol, shortCast, longCast, _colorTurn))
                // the move was confirmed and executed
            {
                _colorTurn = !_colorTurn;
                if (checkInLastTurn)
                {
                    _gameBoard.resetCheck();
                }
                checkInLastTurn = _gameBoard.getIsCheck();
            }
        }
        else
        {
            _gameBoard.printIllegal(); // not in the board limits (also for invalid input)
        }
    }
    _gameBoard.setWinner(_playersName[_gameBoard.getWinnerColor()]); //tells board the winner's name
    // for
    //print uses
    std::cout <<  _gameBoard;
    return RAN_SUCCESSFULLY;
}