//
// Created by Natali on 06/01/2018.
//

#ifndef CHESS_KNIGHT_H
#define CHESS_KNIGHT_H

#include "Piece.h"

/**
 * Class that represents knight piece on chess board
 */
class Knight : public Piece
{
public:
    /**
     * Knight constructor
     * @param color knight color
     * @param row int represents row coordinate
     * @param column int represents column coordinate
     */
    Knight(const Color color, const int row, const int column);

    /**
     * Knight copy constructor
     * @param otherK other knight to be cloned
     */
    Knight(const Knight& otherK);

    /**
     * This function checks if new coordinates of current piece makes a legit movement on the board
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return true if it is a legit move, false otherwise
     */
    bool isLegitMove(const Board& board, const int row, const int column) const;

    /**
     * This function changes current coordinates of any piece on the board to the new given position
     * @param row new row coordinate
     * @param column ne column coordinate
     * @return true if change ended successfully
     */
    bool makeMove(const int row, const int column);

    /**
     * Creates identical new knight (saved on the heap)
     * @return a pointer to the new knight.
     */
    Knight* clone() const override;
};


#endif //CHESS_KNIGHT_H
