file(REMOVE_RECURSE
  "CMakeFiles/chess.dir/chess.cpp.o"
  "CMakeFiles/chess.dir/Piece.cpp.o"
  "CMakeFiles/chess.dir/Pawn.cpp.o"
  "CMakeFiles/chess.dir/Board.cpp.o"
  "CMakeFiles/chess.dir/EmptySpot.cpp.o"
  "CMakeFiles/chess.dir/Bishop.cpp.o"
  "CMakeFiles/chess.dir/Knight.cpp.o"
  "CMakeFiles/chess.dir/Rook.cpp.o"
  "CMakeFiles/chess.dir/King.cpp.o"
  "CMakeFiles/chess.dir/Game.cpp.o"
  "CMakeFiles/chess.dir/Queen.cpp.o"
  "chess.pdb"
  "chess.exe"
  "libchess.dll.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/chess.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
