//
// Created by Natali on 02/01/2018.
//

#ifndef CHESS_PIECE_H
#define CHESS_PIECE_H

#include "Board.h"
#include <string>
class Board;

#define PURE_VIRTUAL 0
#define EMPTY_DEFAULT 0

/**
 * Enum that represents possible colors for each piece on board: WHITE, BALCK, and EMPTY_SPOT
 */
enum Color
{   WHITE,
    BLACK,
    EMPTY_SPOT
};

/**
 * Class that represents Piece on the chess board.
 * this class is abstract and cannot have instances, it's mainly used as a skeleton for "
 * real pieces" like pawn, queen, king etc
 */
class Piece
{

/* Protected (inherited) fields and methods */
protected:
    /* fields */
    int _row, _col, _backGround;
    Color _myColor;
    bool _ifFirst;
    std::string _colorCode;
    std::string _representationCode;

    /* methods */

    /**
     * Checks all the squares between the piece to it's destination on the board,
     * only if the movement is vertical
     * @param board chess board
     * @param destRow int represents the destination's row. the only coordinate that is changed is
     *          the row because the movement is vertical, the column stays the same
     * @return true if some piece stands between current position and destination (not include the
     *          destination square itself), false otherwise (if the way is clear)
     */
    bool _checkVertical(const Board& board, const int destRow) const;

    /**
     * Checks all the squares between the piece to it's destination on the board,
     * only if the movement is Horizontal
     * @param board chess board
     * @param destCol int represents the destination's column. the only coordinate that changed is
     *          the column because the movement is horizontal, the row stays the same
     * @return true if some piece stands between current position and destination (not include the
     *          destination square itself), false otherwise (if the way is clear)
     */
    bool _checkHorizontal(const Board& board, const int destCol) const;

    /**
     * Checks all the squares between the piece to it's destination on the board,
     * only if the movement is Diagonal
     * @param board chess board
     * @param destRow int represents the destination's row
     * @param destCol int represents the destination's column
     * @return true if some piece stands between current position and destination (not include the
     *          destination square itself), false otherwise (if the way is clear)
     */
    bool _checkDiagonal(const Board& board, const int destRow, const int destCol) const;

/* public fields and methods */
public:
    /**
     * Constructor that initiates Piece on the board
     * @param color piece's color
     * @param row piece's row coordinate
     * @param column piece's column coordinate
     */
    Piece(const Color color, const int row, const int column);

    /**
     * This function changes current coordinates of any piece on the board to the new given position
     * @param row new row coordinate
     * @param column ne column coordinate
     * @return true if change ended successfully
     */
    virtual bool makeMove(const int row, const int column) = PURE_VIRTUAL;

    /**
     * This function checks if new coordinates of current piece makes a legit movement on the board
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return true if it is a legit move, false otherwise
     */
    virtual bool isLegitMove
            (const Board& board, const int row, const int column) const = PURE_VIRTUAL;

    /**
     * This function clones the current piece (deep copy)
     * @return pointer to cloned piece
     */
    virtual Piece* clone() const  = PURE_VIRTUAL;

    /**
     * Output operator, prints the piece into the given writable-stream
     * @param os output stream
     * @param p piece to print
     * @return reference to the given output stream
     */
    friend std::ostream& operator << (std::ostream& os, const Piece* p);

    /**
     * Gets the row coordinate of the piece
     * @return int represent the row coordinate
     */
    int row() const;

    /**
     * Gets the column coordinate of the piece
     * @return int represent the column coordinate
     */
    int col() const;

    /**
     * @return Gets the code for string representation of the piece
     */
    std::string getRepresentation() const;

    /**
     * @return true if it's piece's first move, false otherwise
     */
    bool isFirstMove() const;

    /**
     * @return Gets the piece's color
     */
    Color color() const;

    /**
     * sets the pieces current backGround
     * @param backGround int represents the color of the background in unicode
     */
    void setBackground(const int backGround);

    /**
     * Piece's destructor
     */
    virtual ~Piece() {};
};


#endif //CHESS_PIECE_H
