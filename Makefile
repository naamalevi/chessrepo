
CXX = g++ -g
CPPFLAGS = -c -DNDEBUG -Wall -Wextra -Wvla -pthread -std=c++17
TARFILES = Bishop.cpp Bishop.h Board.cpp Board.h chess.cpp EmptySpot.cpp EmptySpot.h Game.cpp Game.h King.cpp King.h Knight.cpp Knight.h Pawn.cpp Pawn.h Piece.cpp Piece.h Queen.cpp Queen.h Rook.cpp Rook.h README Makefile

# All Targets
all: chess

# Executables
chess: Bishop.o Queen.o Rook.o Pawn.o Knight.o King.o Board.o Game.o Piece.o EmptySpot.o chess.o
	$(CXX) $ Bishop.o Queen.o Rook.o Pawn.o Knight.o King.o Board.o Game.o Piece.o EmptySpot.o chess.o -o chess
	
# Objects Files
Bishop.o : Bishop.h Bishop.cpp
	$(CXX) $(CPPFLAGS) Bishop.cpp -o Bishop.o

Queen.o : Queen.h Queen.cpp
	$(CXX) $(CPPFLAGS) Queen.cpp -o Queen.o
	
Rook.o : Rook.h Rook.cpp
	$(CXX) $(CPPFLAGS) Rook.cpp -o Rook.o
	
Pawn.o : Pawn.h Pawn.cpp
	$(CXX) $(CPPFLAGS) Pawn.cpp -o Pawn.o

Knight.o : Knight.h Knight.cpp
	$(CXX) $(CPPFLAGS) Knight.cpp -o Knight.o
	
King.o : King.h King.cpp
	$(CXX) $(CPPFLAGS) King.cpp -o King.o

Board.o : Board.h Board.cpp
	$(CXX) $(CPPFLAGS) Board.cpp -o Board.o

Game.o : Game.h Game.cpp
	$(CXX) $(CPPFLAGS) Game.cpp -o Game.o
	
Piece.o : Piece.h Piece.cpp
	$(CXX) $(CPPFLAGS) Piece.cpp -o Piece.o
	
EmptySpot.o : EmptySpot.h EmptySpot.cpp
	$(CXX) $(CPPFLAGS) EmptySpot.cpp -o EmptySpot.o

tar:
	tar cvf chess.tar $(TARFILES)

chess.o : chess.cpp
	$(CXX) $(CPPFLAGS) chess.cpp -o chess.o


# clean
clean:
	rm -f *.o chess
