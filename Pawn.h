//
// Created by Natali on 05/01/2018.
//

#ifndef CHESS_PAWN_H
#define CHESS_PAWN_H


#include "Piece.h"
#include "Queen.h"

#define PAWNS 8

/**
 * Class reoresents a pawn piece on chess board
 */
class Pawn : public Piece
{
private:
    Queen _queen;
    bool _isQuin;
public:

    /**
     * Pawn constructor
     * @param color pawn color
     * @param row pawn row coordinate
     * @param column pawn column coordinate
     */
    Pawn(const Color color, const int row, const int column);

    /**
     * Pawn copy constructor
     * @param otherP Pawn to clone
     */
    Pawn(const Pawn& otherP);

    /**
     * This function checks if new coordinates of current piece makes a legit movement on the board
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return true if it is a legit move, false otherwise
     */
    bool isLegitMove(const Board& board, const int row, const int column) const;

    /**
    * This function changes current coordinates of any piece on the board to the new given position
    * @param row new row coordinate
    * @param column ne column coordinate
    * @return true if change ended successfully
    */
    bool makeMove(const int row, const int column);

    /**
     * @return pointer to cloned Pawn
     */
    Pawn* clone() const override;
};


#endif //CHESS_PAWN_H
