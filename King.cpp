//
// Created by Natali on 06/01/2018.
//

#include "King.h"
#define STEP 1
#define KING_CODE "\u265A"

/**
 * Kings constructor
 * @param color kings color
 * @param row int represents row coordinate
 * @param column int represents column coordinate
 */
King::King(const Color color, const int row, const int column) : Piece(color, row, column)
{
    _representationCode = KING_CODE;
}

/**
 * King copy constructor
 * @param otherKing other king to be cloned
 */
King::King(const King &otherKing) :
        Piece(otherKing._myColor, otherKing._row, otherKing._col)
{
    _representationCode = otherKing._representationCode;
    _ifFirst = otherKing._ifFirst;
}

/**
 * This function checks if new coordinates of current piece makes a legit movement on the board
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return true if it is a legit move, false otherwise
 */
bool King::isLegitMove(const Board& /*board*/, const int row, const int column) const
{
    int _rowDiff = (row > _row) ? (row - _row) : (_row - row);
    int _colDiff = (column > _col) ? (column - _col) : (_col - column);
    return !((_rowDiff > STEP) || (_colDiff > STEP));

}

/**
 * This function changes current coordinates of any piece on the board to the new given position
 * @param row new row coordinate
 * @param column ne column coordinate
 * @return true if change ended successfully
 */
bool King::makeMove(const int row, const int column)
{
    _ifFirst = false;
    _col = column;
    _row = row;
    return true;
}

/**
 * Creates identical new king (saved on the heap)
 * @return a pointer to the new king.
 */
King* King::clone()const
{
    King* cKingPtr = new King(*this);
    return cKingPtr;
}