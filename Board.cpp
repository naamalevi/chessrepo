//
// Created by Natali on 05/01/2018.
//

#include "Board.h"
#include "Pawn.h"
#include "Knight.h"
#include "King.h"
#include "EmptySpot.h"

#define BLACK_TOOLS 7
#define BLACK_PAWNS 6
#define EMPTY_ROWS 4
#define FIRST_EMPTY_ROW 2
#define WHITE_PAWNS 1
#define WHITE_TOOLS 0
#define FIRST_COL 0
#define LAST_COL 7
#define FIRST_ROW 0
#define LAST_ROW 7

#define SIZE 8
#define CLOSER_ROOK_COL 0
#define L_KNIGHT 1
#define L_BISHOP 2
#define KING 3
#define QUIN 4
#define R_BISHOP 5
#define R_KNIGHT 6
#define FURTHER_ROOK_COL 7
#define BLACK_KING_FIRST_ROW 7
#define WHITE_KING_FIRST_ROW 0

#define ASCII_ESCAPE "\33["
#define RESET_COLOR "0"
#define WHITE_TEXT "37"
#define BLACK_TEXT "30"
#define GREEN_BG 42
#define BLUE_BG 46
#define RED_BG 41
#define E_COLOR_CODE "m"
#define ILLEGAL_MASSAGE "illegal move"
#define COLUMN_INDEX "  ABCDEFGH"
#define CHECK_MASSAGE "Check!"
#define WIN_MASSAGE " won!"
#define SPACE " "
#define NUM_OF_SPECIAL_PIECE_FOR_EACH_PLAYER 2
#define WHITE_KING_IDX 0
#define BLACK_KING_IDX 1
#define STEP 1

//------------------------------------ constructors and destructors -------------------------------

/**
 * Board constructor.
 * Initializes the board according to the first state of the chess board.
 */
Board::Board()
{
    //////////////// initiates black pawns  ////////////////
    for(int p = 0 ; p < PAWNS ; p++)
    {
        _board[BLACK_PAWNS][p] = new Pawn(BLACK, BLACK_PAWNS, p);
    }
    //////////////// initiates white pawns  ////////////////
    for(int w = 0 ; w < PAWNS ; w++)
    {
        _board[WHITE_PAWNS][w] = new Pawn(WHITE, WHITE_PAWNS, w);
    }
    //////////////// initiates empty spots ////////////////
    for(int e = 0 ; e < EMPTY_ROWS ; e++)
    {
        for(int spot = 0 ; spot < SIZE ; spot++)
        {
            _board[FIRST_EMPTY_ROW + e][spot] = new EmptySpot(EMPTY_SPOT, e, spot); //todo: i changed
        }
    }

    int rooks[NUM_OF_SPECIAL_PIECE_FOR_EACH_PLAYER] = {FURTHER_ROOK_COL, CLOSER_ROOK_COL};
    int knights[NUM_OF_SPECIAL_PIECE_FOR_EACH_PLAYER] = {R_KNIGHT, L_KNIGHT};
    int bishops[NUM_OF_SPECIAL_PIECE_FOR_EACH_PLAYER] = {R_BISHOP, L_BISHOP};

    //////////////// initiates black tools on the board ////////////////
    for (int r : rooks)
    {
        _board[BLACK_TOOLS][r] = new Rook(BLACK, BLACK_TOOLS, r);
    }
    for (int k : knights)
    {
        _board[BLACK_TOOLS][k] = new Knight(BLACK, BLACK_TOOLS, k);
    }
    for (int b : bishops)
    {
        _board[BLACK_TOOLS][b] = new Bishop(BLACK, BLACK_TOOLS, b);
    }
    _board[BLACK_TOOLS][KING] = new King(BLACK, BLACK_TOOLS, KING);
    _board[BLACK_TOOLS][QUIN] = new Queen(BLACK, BLACK_TOOLS, QUIN);

    _kingLocations[1] = _board[BLACK_TOOLS][KING];
    //////////////// initiates white tools on the board ////////////////
    for (int r : rooks)
    {
        _board[WHITE_TOOLS][r] = new Rook(WHITE, WHITE_TOOLS, r);
    }
    for (int k : knights)
    {
        _board[WHITE_TOOLS][k] = new Knight(WHITE, WHITE_TOOLS, k);
    }
    for (int b : bishops)
    {
        _board[WHITE_TOOLS][b] = new Bishop(WHITE, WHITE_TOOLS, b);
    }
    _board[WHITE_TOOLS][KING] = new King(WHITE, WHITE_TOOLS, KING);
    _board[WHITE_TOOLS][QUIN] = new Queen(WHITE, WHITE_TOOLS, QUIN);

    _kingLocations[0] = _board[WHITE_TOOLS][KING];
    _winnerName = EMPTY_STR;
    _isCheck = _isCheckMate = false;
}


/**
 * Board destructor.
 * Releases the inner located memory.
 */
Board::~Board()
{
    for(int r = 0 ; r < SIZE ; r++)
    {
        for(int c = 0 ; c < SIZE ; c++)
        {
            delete _board[r][c];
        }
    }
}


/**
 * Copy constructor. Creates a new identical Board as the given board.
 * @param boardToCopy - A board to deep copy.
 */
Board::Board(const Board& boardToCopy)
{
    int row, col;
    for(row = 0; row < SIZE ; row++)
    {
        for(col = 0; col < SIZE ; col++)
        {
            _board[row][col] = boardToCopy(row, col) -> clone();
        }
    }
    _kingLocations[WHITE_KING_IDX] =
            _board[boardToCopy._getKingRow(false)][boardToCopy._getKingCol(false)];
    _kingLocations[BLACK_KING_IDX] =
            _board[boardToCopy._getKingRow(true)][boardToCopy._getKingCol(true)];
    _winnerName = boardToCopy._winnerName;
    _isCheck = boardToCopy._isCheck;
    _isCheckMate = boardToCopy._isCheckMate;
    _winnersColor = boardToCopy._winnersColor;
}

//------------------------------------ operators overloading ------------------------------------
/**
 * Overloads the assignment operator.
 * Do deep assignment, makes a deep copy of the board.
 * @param other - A board to deep copy.
 * @return - A ref to the given board, contains the deep copied data.
 */
Board& Board::operator = (const Board &other)
{
    if (this != &other)
    {
        int row, col;
        for (row = 0; row < SIZE; row++)
        {
            for (col = 0; col < SIZE; col++)
            {
                delete _board[row][col];
                _board[row][col] = other(row, col) -> clone();
            }
        }
        _kingLocations[false] = _board[other._getKingRow(false)][other._getKingCol(false)];
        _kingLocations[true] = _board[other._getKingRow(true)][other._getKingCol(true)];
        _winnerName = other._winnerName;
        _isCheck = other._isCheck;
        _isCheckMate = other._isCheckMate;
        _winnersColor = other._winnersColor;
    }
    return *this;
}


/**
 * Overload the () operator, returns a pointer to the piece ( chess tool or empty spot)
 * that is held in the given location.
 * @param row - An int representing the row location.
 * @param col- An int representing the column location.
 * @return a pointer to the tool that is held is that location.
 */
Piece* Board::operator () (const int &row, const int &col) const
{
    return _board[row][col];
}


//------------------------------------- Getters & Setters ----------------------------------------

/**
 * Updates the winnerName field.
 * @param winner A string representing the winner's name.
 */
void Board::setWinner(const std::string &winner)
{
    _winnerName = winner;
}
/**
 * Updates the isCheck flag to represent false - no check.
 */
void Board::resetCheck()
{
    _isCheck = false;
}

/**
 * A getter - returns the winner's name.
 * @return a string thet representing the winner's name.
 */
std::string Board::getWinner() const
{
    return _winnerName;
}
/**
 * A getter - returns the current isCheck status.
 * @return A bool represents if there is a check on the board.
 */
bool Board::getIsCheck() const
{
    return _isCheck;
}


/**
 * A getter - returns the current isCheckMate status.
 * @return A bool represents if there is a checkMate on the board.
 */
bool Board::getIsCheckMate() const
{
    return _isCheckMate;
}

/**
 * A getter - returns the color of the winner.
 * @return An boolean represents the color of the winner - true for black, 0 for white.
 */
bool Board::getWinnerColor() const
{
    return _winnersColor;
}

/**
 * Returns the row location of the king that it asked to.
 * @param isBlack - A boolean representing the color of the wanted king, true for black,
 * false for white.
 * @return - An int representing the row location of the king in the given color.
 */
int Board::_getKingRow(const bool &isBlack) const
{
    return _kingLocations[isBlack][0].row();
}


/**
 * Returns the column location of the king that it asked to.
 * @param isBlack - A boolean representing the color of the wanted king, true for black,
 * false for white.
 * @return - An int representing the column location of the king in the given color.
 */
int Board::_getKingCol(const bool &isBlack) const
{
    return _kingLocations[isBlack][0].col();
}


//------------------------------------- Print functions  ------------------------------------------

/**
  * Overload the << operator, prints the board in the classic board format, when blue is the top
  * right piece color. In case of check or win situation the massages are printed below the
  * board.
  * @param board - A raf to board to print.
  * @return  A ref to the given ostream
  */
std::ostream& operator << (std::ostream& os, const Board& board)
{
    int background, pRow, pCol;
    os << COLUMN_INDEX << std::endl << std::endl;

    for (pRow = SIZE; pRow > 0; pRow--)
    {
        os << pRow << SPACE; // creates the right row index
        for (pCol = SIZE; pCol > 0; pCol --)
        {
            background = ((pRow + pCol) % 2 == 0) ? BLUE_BG : GREEN_BG;
            board(pRow - STEP, pCol - STEP) -> setBackground(background);
            os << board(pRow - STEP, pCol - STEP);
        }
        os << SPACE << pRow << std::endl; // creates the left row index
    }

    os << std::endl << COLUMN_INDEX << std::endl << std::endl;

    if (board.getIsCheck())
    {

        os << ASCII_ESCAPE << WHITE_TEXT << ";" << RED_BG << E_COLOR_CODE << CHECK_MASSAGE
           << ASCII_ESCAPE << RESET_COLOR << E_COLOR_CODE << std::endl;
    }
    if (!board.getWinner().empty())
    {
        os << board.getWinner() << WIN_MASSAGE << std::endl;
    }
    return os;
}

/**
 * Prints an illegal message to the board, (white letters on red background).
 */
void Board::printIllegal() const
{
    std::cout << ASCII_ESCAPE << WHITE_TEXT << ";" << RED_BG << E_COLOR_CODE << ILLEGAL_MASSAGE <<
              ASCII_ESCAPE << RESET_COLOR << E_COLOR_CODE << std::endl;
}

//------------------------------------- move tools methods ---------------------------------------

/**
 * Control the move operation.
 * If the move is legal, updates the game state: board, check, chackmate and winner's color.
 * otherwise prints a massage to the board.
 * @param sourceRow - const ref to int, row location of the tool that the player want to move.
 * @param sourceCol - const ref to int, column location of the tool that the player want to move.
 * @param destRow - const ref to int, row location that the player want to move its tool to.
 * @param destCol - const ref to int, column location that the player want to move its tool to.
 * @param isShortCast - A ref to boolean representing if the given move is a short castling.
 * @param isLongCast - A ref to boolean representing if the given move is a long castling.
 * @param colorTurn - A ref to boolean representing the current color thet plays, 1 for b, 0 for w.
 * @return true if the move (legal and) was done' false otherwise.
 */
bool Board::moveOperation(const int &sourceRow, const int &sourceCol, const int &destRow,
                          const int &destCol, const bool &isShortCast, const bool &isLongCast,
                          const bool &colorTurn)
{
    // duplicates the board in order to check if the move will not put the current player's
    // king in a risk:
    Board optionalBoard = *this;

    // checks the first checks- the move is legal, with no care for the consequences.
    bool castlingApproved = !(isShortCast || isLongCast) &&
                            optionalBoard._confirmNormalMove(sourceRow, sourceCol, destRow, destCol,
                                                             colorTurn);
    bool shortCastlingApproved = isShortCast && optionalBoard._confirmShortCastlingMove(colorTurn);
    bool longCastlingApproved = isLongCast && optionalBoard._confirmLongCastlingMove(colorTurn);

    int kingRow;
    int kingCol;

    bool isThreatened;
    bool shortCastApprovedAndIsThreatened;
    bool longCastApprovedAndIsThreatened;

    if (castlingApproved || shortCastlingApproved || longCastlingApproved)
    {
        kingRow = optionalBoard._getKingRow(colorTurn);
        kingCol = optionalBoard._getKingCol(colorTurn);

        // If at the end of the turn, the king of the current player is threatened (in case of
        // castling also swapped rook cant be threatened) - the move is
        // illegal and the optionalBoard is not valid, otherwise the move is legal and been saved.
        isThreatened = !optionalBoard._checkIfThreatened(kingRow, kingCol, colorTurn);

        shortCastApprovedAndIsThreatened = isShortCast &&
                                           !optionalBoard._checkIfThreatened(kingRow, kingCol,
                                                                             colorTurn) &&
                                           !optionalBoard._checkIfThreatened(CLOSER_ROOK_COL,
                                                                             kingCol, colorTurn);

        longCastApprovedAndIsThreatened = isLongCast &&
                                          !optionalBoard._checkIfThreatened(kingRow, kingCol,
                                                                            colorTurn) &&
                                          !optionalBoard._checkIfThreatened(FURTHER_ROOK_COL,
                                                                            kingCol, colorTurn);

        if ( isThreatened || shortCastApprovedAndIsThreatened || longCastApprovedAndIsThreatened )
        {
            *this = optionalBoard;
            int oKingRow = _getKingRow(!colorTurn); //the turn is not already been modified
            int oKingCol = _getKingCol(!colorTurn);

            // checks the consequences of this move on the -other player- risks.
            if (_checkIfThreatened(oKingRow, oKingCol, !colorTurn))
            {
                _isCheck = true;
                if (_checkIfCheckMate(oKingRow, oKingCol, !colorTurn))
                {
                    _isCheckMate = true;
                    _isCheck = false;
                    _winnersColor = colorTurn;

                }
            }
            return true;
        }
    }
    printIllegal(); //the move is illegal (although that in the correct format)
    return false;
}


/**
 * Checks if the given castling type is supported according to the board state (assuming the
 * first checks had already done, there are tools in the respectively locations, and these tools
 * are in the current turn color. Also, the king is not threatened.
 * The consequence checks are not be checked in this method.
 * @param row - An int representing the row that the wanted castling should be done in.
 * @param shortCastling - An boolean, true for short castling, false for long one.
 * @return True if this castling is legal for this conditions, false otherwise.
 */
bool Board::_isLegitCastling(const int &row, const bool &shortCastling) const
{
    if((row == BLACK_TOOLS) && _board[BLACK_TOOLS][KING] -> isFirstMove())
    {
        // checks for short castling, if left rook didn't move yet and if there are no tools across
        // (mirror shown)
        if(shortCastling && _board[BLACK_TOOLS][CLOSER_ROOK_COL] -> isFirstMove() &&
           _board[BLACK_TOOLS][CLOSER_ROOK_COL] -> isLegitMove(*this, BLACK_TOOLS, KING))
        {
            return true;
        }
        else
        {
            // checks for long castling, if right rook didn't move yet and if there are no tools
            // across (mirror shown)
            return !shortCastling && _board[BLACK_TOOLS][FURTHER_ROOK_COL] -> isFirstMove() &&
                   _board[BLACK_TOOLS][FURTHER_ROOK_COL] -> isLegitMove(*this, BLACK_TOOLS, KING);
        }

    }
    else if ((row == WHITE_TOOLS) && _board[WHITE_TOOLS][KING] -> isFirstMove()) // white case
    {
        if(shortCastling && _board[WHITE_TOOLS][CLOSER_ROOK_COL] -> isFirstMove() &&
           _board[WHITE_TOOLS][CLOSER_ROOK_COL] -> isLegitMove(*this, WHITE_TOOLS, KING))
        {
            return true;
        }
        else
        {
            return !shortCastling && _board[WHITE_TOOLS][FURTHER_ROOK_COL] -> isFirstMove() &&
                   _board[WHITE_TOOLS][FURTHER_ROOK_COL] -> isLegitMove(*this, WHITE_TOOLS, KING);
        }
    }
    return false;
}


/**
 * Moves a castling if it legal (without treatment in the consequences)
 * @param row - An int representing the row that the wanted castling should be done in.
 * @param shortCastling - An boolean, true for short castling, false for long one.
 * @return True if this castling is legal and was done, false otherwise.
 */
bool Board::_moveCastling(const int &row, const bool &shortCastling)
{
    if (_isLegitCastling(row, shortCastling))
    {
        if (shortCastling)
        {

            delete _board[row][KING - STEP];
            delete _board[row][KING - 2 * STEP];

            _board[row][CLOSER_ROOK_COL] -> makeMove(row, CLOSER_ROOK_COL + 2 * STEP); // updates the spots coordinates fields
            _board[row][KING] -> makeMove(row, KING - 2 * STEP);
            _board[row][KING - STEP] = _board[row][CLOSER_ROOK_COL]; // updates the coordinates on the board
            _board[row][KING - 2 * STEP] = _board[row][KING];
            _board[row][CLOSER_ROOK_COL] = new EmptySpot(EMPTY_SPOT, row, CLOSER_ROOK_COL);
            _board[row][KING] = new EmptySpot(EMPTY_SPOT, row, KING);
        }
        else // long castling
        {
            delete _board[row][KING + STEP];
            delete _board[row][KING + 2 * STEP];

            _board[row][FURTHER_ROOK_COL] -> makeMove(row, FURTHER_ROOK_COL - 3 * STEP);  // updates the spots coordinates fields
            _board[row][KING] -> makeMove(row, KING + 2 * STEP);
            _board[row][KING + STEP] = _board[row][FURTHER_ROOK_COL]; // updates the coordinates on the board
            _board[row][KING + 2 * STEP] = _board[row][KING];
            _board[row][FURTHER_ROOK_COL] = new EmptySpot(EMPTY_SPOT, row, FURTHER_ROOK_COL);
            _board[row][KING] = new EmptySpot(EMPTY_SPOT, row, KING);
        }
        return true;
    }
    return false;
}


/**
 * Moves a normal move if it legal (without treatment in the consequences)
 * @param iRow - const int, row location of the tool that the player want to move.
 * @param iCol - const int, column location of the tool that the player want to move.
 * @param dRow - const to int, row location that the player want to move its tool to.
 * @param dCol - const to int, column location that the player want to move its tool to.
 * @return True if this move is legal and was done, false otherwise.
 */
bool Board::_moveTools(const int &iRow, const int &iCol, const int &dRow, const int &dCol)
{
    if(_board[iRow][iCol]->isLegitMove(*this, dRow, dCol))
    {
        delete _board[dRow][dCol];
        _board[iRow][iCol] -> makeMove(dRow, dCol);  // updates the spots coordinates fields
        _board[dRow][dCol] = _board[iRow][iCol];  // updates the coordinates on the board
        _board[iRow][iCol] = new EmptySpot(EMPTY_SPOT, iRow, iCol);
        return true;
    }
    return false;
}


/**
 * Checks if the move can be done, according to the game rules (with no treatment to the
 * consequences), in case of legal move - updates the board.
 * return true if all the conditions to non-castling move are carried on (there is a tool in that
 * location, the tool is in the current turn color, the destination of the move is a new location
 * on the board, with no self color tool that stands there, and the move is valid for the chosen
 * type of tool.
 * @param sourceRow - const ref to int, row location of the tool that the player want to move.
 * @param sourceCol - const ref to int, column location of the tool that the player want to move.
 * @param destRow - const ref to int, row location that the player want to move its tool to.
 * @param destCol - const ref to int, column location that the player want to move its tool to.
 * @return true if the move is legal (with no treatment to the consequences) and was done on the
 * given board
 */
bool Board::_confirmNormalMove(const int &sourceRow, const int &sourceCol,
                               const int &destRow, const int &destCol, bool turnColor)
{
    Color toolColor = (turnColor) ?  BLACK : WHITE;
    return (((_board[sourceRow][sourceCol])->color() == toolColor) &&
            ((sourceRow != destRow) || (sourceCol != destCol)) &&
            ((_board[destRow][destCol]) -> color() != toolColor) &&
            (_moveTools(sourceRow, sourceCol, destRow, destCol)));
}

/**
 * Checks if the short castling can be done, according to the game rules (with no treatment to the
 * consequences), in case of legal move - updates the board.
 * return true if all the conditions to short castling are carried on (the king and the rook
 * that closer to the king are there, didn't moved before, there are no tools between them, and
 * the king is not threatened.
 * @param turnColor - An boolean represents the color that plays now, 1 for B, 0 for W.
 * @return true if the castling is legal (with no treatment to the consequences) and was done on the
 * given board
 */
bool Board::_confirmShortCastlingMove(bool turnColor)
{
    int kingRow = (turnColor)? BLACK_KING_FIRST_ROW : WHITE_KING_FIRST_ROW;
    Color toolsColor = (turnColor) ?  BLACK : WHITE;
    return ((_board[kingRow][KING] -> color() == toolsColor) &&
            (_board[kingRow][CLOSER_ROOK_COL] -> color() == toolsColor) &&
            (!_isCheck) && _moveCastling(kingRow, true));
}


/**
 * Checks if the long castling can be done, according to the game rules (with no treatment to the
 * consequences), in case of legal move - updates the board.
 * return true if all the conditions to long castling are carried on (the king and the rook
 * that further to the king are there, didn't moved before, there are no tools between them, and
 * the king is not threatened.
 * @param turnColor - An boolean represents the color that plays now, 1 for B, 0 for W.
 * @return true if the castling is legal (with no treatment to the consequences) and was done on the
 * given board
 */
bool Board::_confirmLongCastlingMove(bool turnColor)
{
    int kingRow = (turnColor)? BLACK_KING_FIRST_ROW : WHITE_KING_FIRST_ROW;
    Color toolsColor = (turnColor) ?  BLACK : WHITE;
    return ((_board[kingRow][KING] -> color() == toolsColor) &&
            (_board[kingRow][FURTHER_ROOK_COL] -> color() == toolsColor) && (!_isCheck) &&
            _moveCastling(kingRow, false));
}

//----------------------------- check move's consequences methods -----------------------------

/**
 * Checks if the given location on the board is threatened, that is to say can be eaten by any
 * tool of the opponent chess tools.
 * @param row - An int representing the row location that we want to check if threatened.
 * @param col - An int representing the column location that we want to check if threatened.
 * @param isBlackTurn - A boolean representing the current turn-color, true for black, false for
 * white.
 * @return A boolean, true if the piece is threatened, false otherwise.
 */
bool Board::_checkIfThreatened(const int &row, const int &col, bool isBlackTurn) const
{
    int pRow, pCol;
    Color opponentColor = (isBlackTurn)? WHITE : BLACK;
    for(pRow = 0; pRow < SIZE ; pRow ++)
    {
        for(pCol = 0; pCol < SIZE ; pCol ++)
        {
            // checks if there is a tool in the opposite color that can "eat" the given spot
            if ((_board[pRow][pCol] -> color() == opponentColor) &&
                (_board[pRow][pCol] -> isLegitMove(*this, row, col)))
            {
                return true;
            }
        }
    }
    return false;
}


/**
 * Check if all the empty spots around the given spot (within the board limits) are threatened by
 * the opponent tools.
 * @param row - An int representing the row location that we want to check if all the empty
 * spots around it are threatened.
 * @param col - An int representing the column location that we want to check if empty
 * spots around it are threatened.
 * @param isBlackTurn - A boolean representing the current turn-color, true for black, false for
 * white.
 * @return A boolean, true if all the empty spots around it are threatened, false otherwise.
 */
bool Board::_checkIfCheckMate(const int &row, const int &col, bool isBlackTurn) const
{
    Color playerColor = (isBlackTurn)? BLACK : WHITE ;
    int sRow, eRow, sCol, eCol, sColIter, sRowIter;
    sRow = (FIRST_ROW > row - STEP) ? FIRST_ROW : row - STEP;
    eRow = (row + STEP > LAST_ROW) ? LAST_ROW : row + STEP;
    sCol = (FIRST_COL > col - STEP) ? FIRST_COL : col - STEP;
    eCol = (col + STEP > LAST_COL) ? LAST_COL : col + STEP;
    for(sRowIter = sRow; sRowIter <= eRow ; sRowIter++)
    {
        for(sColIter = sCol; sColIter <= eCol ; sColIter++)
        {
            if (_board[sRowIter][sColIter] -> color() != playerColor)
            {
                // duplicates the board to check whether after the optional move the king is in risk
                Board afterKingMove = *this;
                afterKingMove._moveTools(row, col, sRowIter, sColIter);
                if (!afterKingMove._checkIfThreatened(sRowIter, sColIter, isBlackTurn))
                {
                    return false;
                }
            }
        }
    }
    return true;
}
