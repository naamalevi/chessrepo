//
// Created by Natali on 06/01/2018.
//

#include "Queen.h"
#define QUEEN_CODE "\u265B"

/**
 * Queen constructor
 * @param color queen color
 * @param row int represents row coordinate
 * @param column int represents column coordinate
 */
Queen::Queen(const Color color, const int row, const int column) :
        Piece(color, row, column) , _rook(color, row, column), _bishop(color, row, column)
{
    _representationCode = QUEEN_CODE;
}


/**
 * Queen copy constructor
 * @param otherQ other queen to be cloned
 */
Queen::Queen(const Queen& otherQ): Piece(otherQ._myColor, otherQ._row, otherQ._col) ,
                                   _rook(otherQ._rook), _bishop(otherQ._bishop)
{
    _representationCode = otherQ._representationCode;
    _ifFirst = otherQ._ifFirst;
}


/**
 * This function checks if new coordinates of current piece makes a legit movement on the board
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return true if it is a legit move, false otherwise
 */
bool Queen::isLegitMove(const Board& board, const int row, const int column) const
{
    return _rook.isLegitMove(board, row, column) || _bishop.isLegitMove(board, row, column);
}

/**
* This function changes current coordinates of any piece on the board to the new given position
* @param row new row coordinate
* @param column ne column coordinate
* @return true if change ended successfully
*/
bool Queen::makeMove(const int row, const int column)
{
    _bishop.makeMove(row, column);
    _rook.makeMove(row, column);
    _ifFirst = false;
    _row = row;
    _col = column;
    return true;
}

/**
 * Creates identical new queen (saved on the heap)
 * @return a pointer to the new queen.
 */
Queen* Queen::clone() const
{
    Queen* cQuinPtr = new Queen(*this);
    return cQuinPtr;
}