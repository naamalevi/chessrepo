//
// Created by Natali on 05/01/2018.
//

#include "Pawn.h"
#define STEP 1
#define BLACK_SPECIAL_ROW 7
#define WHITE_SPECIAL_ROW 0
#define PAWN_CODE "\u265F"

/**
 * Pawn constructor
 * @param color pawn color
 * @param row pawn row coordinate
 * @param column pawn column coordinate
 */
Pawn::Pawn(const Color color, const int row, const int column) : Piece(color, row, column),
                                                                 _queen(color, row, column),
                                                                 _isQuin(false)
{
    _representationCode = PAWN_CODE;
}

/**
 * Pawn copy constructor
 * @param otherP Pawn to clone
 */
Pawn::Pawn(const Pawn &otherP) : Piece(otherP._myColor, otherP._row, otherP._col),
                                 _queen(otherP._queen), _isQuin(otherP._isQuin)
{
    _ifFirst = otherP._ifFirst;
    _representationCode = otherP._representationCode;
}

/**
 * This function checks if new coordinates of current piece makes a legit movement on the board
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return true if it is a legit move, false otherwise
 */
bool Pawn::isLegitMove(const Board& board, const int row, const int column) const
{
    if(_isQuin)
    {
        return _queen.isLegitMove(board, row, column);
    }
    const int rowStep = (_myColor == BLACK) ? -STEP : STEP;
    if(column == _col)
    {
        bool oneStep = (row == _row + rowStep) && !_checkVertical(board, _row + 2 * rowStep);
        bool twoSteps = (row == _row + 2 * rowStep) && _ifFirst &&
                        !_checkVertical(board, _row + 3 * rowStep);
        return oneStep || twoSteps;
    }
    const int colDist = (column > _col) ? (column - _col) : (_col - column);
    if ((colDist == STEP || colDist == -STEP ) && (row == _row + rowStep))
    {
        return ((board(row, column)->color() != _myColor) &&
                board(row, column)->color() != EMPTY_SPOT);
    }
    return false;
}

/**
* This function changes current coordinates of any piece on the board to the new given position
* @param row new row coordinate
* @param column ne column coordinate
* @return true if change ended successfully
*/
bool Pawn::makeMove(const int row, const int column)
{
    _ifFirst = false;
    _queen.makeMove(row, column);
    _col = column;
    _row = row;
    if((_myColor == BLACK && _row == WHITE_SPECIAL_ROW) ||
       (_myColor == WHITE && _row == BLACK_SPECIAL_ROW))
    {
        _isQuin = true;
        _representationCode = _queen.getRepresentation();
    }
    return true;
}

/**
 * Creates identical new pawn (saved on the heap)
 * @return a pointer to the new pawn.
 */
Pawn* Pawn::clone() const
{
    Pawn* cPawnPtr = new Pawn(*this);
    return cPawnPtr;
}
