//
// Created by Natali on 06/01/2018.
//

#include "Knight.h"
#define STEP 1
#define KNIGHT_CODE "\u265E"

/**
 * Knight constructor
 * @param color knight color
 * @param row int represents row coordinate
 * @param column int represents column coordinate
 */
Knight::Knight
        (const Color color, const int row, const int column) : Piece(color, row, column)
{
    _representationCode = KNIGHT_CODE;
}

/**
 * Knight copy constructor
 * @param otherK other knight to be cloned
 */
Knight::Knight(const Knight &otherK) : Piece(otherK._myColor, otherK._row, otherK._col)
{
    _representationCode = otherK._representationCode;
    _ifFirst = otherK._ifFirst;
}

/**
 * This function checks if new coordinates of current piece makes a legit movement on the board
 * @param board chess board
 * @param row new row coordinate
 * @param column new column coordinate
 * @return true if it is a legit move, false otherwise
 */
bool Knight::isLegitMove(const Board& /*board*/, const int row, const int column) const
{
    int rowDiff = (row > _row) ? (row - _row) : (_row - row);
    int colDiff = (column > _col) ? (column - _col) : (_col - column);
    return ((rowDiff == STEP) && (colDiff == 2 * STEP)) ||
            ((rowDiff == 2 * STEP) && (colDiff == STEP));
}

/**
 * This function changes current coordinates of any piece on the board to the new given position
 * @param row new row coordinate
 * @param column ne column coordinate
 * @return true if change ended successfully
 */
bool Knight::makeMove(const int row, const int column)
{
    _ifFirst = false;
    _row = row;
    _col = column;
    return true;
}

/**
 * Creates identical new knight (saved on the heap)
 * @return a pointer to the new knight.
 */
Knight* Knight::clone() const
{
    Knight* cKnightPtr = new Knight(*this);
    return cKnightPtr;
}