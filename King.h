//
// Created by Natali on 06/01/2018.
//

#ifndef CHESS_KING_H
#define CHESS_KING_H

#include "Piece.h"

/**
 * Class that represents king piece on chess board
 */
class King : public Piece
{
public:
    /**
     * Kings constructor
     * @param color kings color
     * @param row int represents row coordinate
     * @param column int represents column coordinate
     */
    King(const Color color, const int row, const int column);

    /**
     * King copy constructor
     * @param otherKing other king to be cloned
     */
    King(const King& otherKing);

    /**
     * This function checks if new coordinates of current piece makes a legit movement on the board
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return true if it is a legit move, false otherwise
     */
    bool isLegitMove(const Board& board, const int row, const int column) const;

    /**
     * This function changes current coordinates of any piece on the board to the new given position
     * @param row new row coordinate
     * @param column ne column coordinate
     * @return true if change ended successfully
     */
    bool makeMove(const int row, const int column);

    /**
     * Creates identical new king (saved on the heap)
     * @return a pointer to the new king.
     */
    King* clone()const override;
};


#endif //CHESS_KING_H
