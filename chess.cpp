#include "Game.h"

/**
 * Main function, runs the program
 * @return 0 if ran successfully, other number otherwise
 */
int main()
{
    Game game;
    return game.runGame();
}
