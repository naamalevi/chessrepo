//
// Created by Natali on 06/01/2018.
//

#ifndef CHESS_BISHOP_H
#define CHESS_BISHOP_H

#include "Piece.h"

/**
 * Class that represents bishop object
 */
class Bishop : public Piece
{
public:
    /**
     * Bishop constructor
     * @param color bishops color
     * @param row row coordinate
     * @param column column coordinate
     */
    Bishop(const Color color, const int row, const int column);

    /**
     * Bishop copy constructor (deep copy)
     * @param otherB bishop to copy
     */
    Bishop(const Bishop& otherB);

    /**
     * This function checks if new coordinates of current piece makes a legit movement on the board
     * @param board chess board
     * @param row new row coordinate
     * @param column new column coordinate
     * @return true if it is a legit move, false otherwise
     */
    bool isLegitMove(const Board& board, const int row, const int column) const;

    /**
     * This function changes current coordinates of any piece on the board to the new given position
     * @param row new row coordinate
     * @param column ne column coordinate
     * @return true if change ended successfully
     */
    bool makeMove(const int row, const int column);

    /**
     * Creates identical new bishop (saved on the heap)
     * @return a pointer to the new bishop.
     */
    Bishop* clone() const override;
};

#endif //CHESS_BISHOP_H
